﻿using Microsoft.EntityFrameworkCore;
using System;

namespace AccesoDatos
{
    public class MyContext : DbContext
    {
        public DbSet<Modelo.Cliente.Cliente> Clientes { get; set; }
        public DbSet<Modelo.Inventario.Residuo> Residuos { get; set; }
        public DbSet<Modelo.Inventario.TipoResiduo> TiposResiduo { get; set; }
        public DbSet<Modelo.Inventario.Almacen> Almacenes { get; set; }
        public DbSet<Modelo.Inventario.UnidadMedida> UnidadesMedida { get; set; }
        public DbSet<Modelo.Persona.Cooperativista> Cooperativistas { get; set; }
        public DbSet<Modelo.Persona.Direccion> Direcciones { get; set; }
        public DbSet<Modelo.Persona.Fisica> PersonasFisicas { get; set; }
        public DbSet<Modelo.Servicio.Bitacora> Bitacoras { get; set; }
        public DbSet<Modelo.Servicio.Contrato> Contratos { get; set; }

        public MyContext(DbContextOptions options):base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(@"host=localhost;port=5432;database=TSMA_TEST;user id=postgres;password=Beallend4all;", b=> b.MigrationsAssembly("Listas"));
        }
    }
}
